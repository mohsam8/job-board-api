class AddJobPostToJobApplication < ActiveRecord::Migration[7.0]
  def change
    add_reference :job_applications, :job_post
  end
end
