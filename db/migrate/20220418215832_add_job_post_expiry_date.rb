class AddJobPostExpiryDate < ActiveRecord::Migration[7.0]
  def change
    add_column :job_posts, :expiry_date, :datetime
  end
end
