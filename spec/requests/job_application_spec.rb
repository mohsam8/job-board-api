require 'rails_helper'

RSpec.describe "/job_applications", type: :request do

  before(:each) do
    User.new(name: "test", password: '123123', email: "sample@example.com", is_admin: false).save
    JobPost.new(title: "test", description: "test").save
    JobApplication.new(job_post_id: 1, user_id: 1).save
  end
  context "GET /index" do
    it "returns a success response" do
      my_headers = { "HTTP_ACCEPT": "application/json", "Authorization": JsonWebToken.encode(user_id: User.find(1).id) }
      get api_v1_job_applications_url, xhr: true, headers: my_headers
      expect(response).to have_http_status(200)
    end
  end

  context "GET /show" do
    it "returns a success response" do
      my_headers = { "HTTP_ACCEPT": "application/json", "Authorization": JsonWebToken.encode(user_id: User.find(1).id) }
      get api_v1_job_applications_url(1), xhr: true, headers: my_headers
      expect(response).to have_http_status(200)
    end
  end
end
