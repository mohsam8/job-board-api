require 'rails_helper'

RSpec.describe "/users", type: :request do

  before(:each) do
    User.new(name: "test", password: '123123', email: "sample@example.com", is_admin: false).save
  end
  context "GET /index" do
    it "returns a success response" do
      my_headers = { "HTTP_ACCEPT": "application/json", "Authorization": JsonWebToken.encode(user_id: User.find(1).id) }
      get api_v1_users_url, xhr: true, headers: my_headers
      expect(response).to have_http_status(200)
    end
  end
  context "GET /show" do
    it "returns a success response" do
      my_headers = { "HTTP_ACCEPT": "application/json", "Authorization": JsonWebToken.encode(user_id: User.find(1).id) }
      get api_v1_user_url(1), xhr: true, headers: my_headers
      expect(response).to have_http_status(200)
    end
  end
end
