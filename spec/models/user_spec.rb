require 'rails_helper'

RSpec.describe User, type: :model do
  context 'Validation test' do
    it 'should be successful' do
      user = User.new(name: "test", password: '123123', email: "sample@example.com", is_admin: false).save
      expect(user).to eq(true)
    end

    it 'ensure email' do
      user = User.new(name: "test", password: '123', is_admin: false).save
      expect(user).to eq(false)
    end

    it 'ensure password' do
      user = User.new(name: "test", email: 'sample@example.com', is_admin: false).save
      expect(user).to eq(false)
    end

    it 'ensure name' do
      user = User.new(email: "sample@example.com", password: '123123', is_admin: false).save
      expect(user).to eq(false)
    end

    it 'ensure is_admin have a default value' do
      user = User.new(name: "test", password: '123123', email: "sample@example.com").save
      expect(user).to eq(true)
    end

  end
end
