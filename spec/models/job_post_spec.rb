require 'rails_helper'

RSpec.describe JobPost, type: :model do
  context 'Validation test' do
    it 'should be successful' do
      job_post = JobPost.new(title: "test", description: 'test').save
      expect(job_post).to eq(true)
    end

    it 'ensure title' do
      job_post = JobPost.new(description: 'test').save
      expect(job_post).to eq(false)
    end

    it 'ensure description' do
      job_post = JobPost.new(title: 'test').save
      expect(job_post).to eq(false)
    end
      end
end
