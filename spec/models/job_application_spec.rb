require 'rails_helper'

RSpec.describe JobApplication, type: :model do
  context 'Validation test' do
    before(:each) do
      User.new(name: "test", password: '123123', email: "sample@example.com", is_admin: false).save
      JobPost.new(title: "test", description: 'test').save
    end
    it 'should be successful' do
      job_application = JobApplication.new(user_id: 1, job_post_id:1).save
      expect(job_application).to eq(true)
    end

    it 'ensure user_id' do
      job_application = JobApplication.new(job_post_id:1).save
      expect(job_application).to eq(false )
    end

    it 'ensure job_post_id' do
      job_application = JobApplication.new(user_id: 1).save
      expect(job_application).to eq(false)
    end

  end
end
