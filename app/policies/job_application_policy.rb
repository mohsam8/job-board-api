class JobApplicationPolicy < ApplicationPolicy
  def index?
    user.is_admin? or record.user == user
  end

  def show?
    user.is_admin? or record.user == user
  end

  def create?
    !user.is_admin?
  end

  def update?
    user.is_admin?
  end

  def destroy?
    record.user == user
  end
end