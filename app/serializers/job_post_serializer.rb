class JobPostSerializer < ActiveModel::Serializer
  attributes :id, :title, :description, :created_at, :expiry_date
  has_many :job_applications
end
