class UserSerializer < ActiveModel::Serializer
  attributes :id, :is_admin, :email, :name
  has_many :job_applications
end
