class JobApplicationSerializer < ActiveModel::Serializer
  attributes :id, :status
  belongs_to :user
  belongs_to :job_post
end
