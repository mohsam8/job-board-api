class JobApplication < ApplicationRecord
  belongs_to :user
  belongs_to :job_post
  enum status: [:not_seen, :seen]
  scope :filter_by_user, -> (user_id) { where user_id: user_id }
  scope :filter_by_status, -> (status) { where status: status }
end


