class User < ApplicationRecord
end
class User < ApplicationRecord
  has_many :job_applications
  has_secure_password
  validates :email, presence: true, uniqueness: true
  validates :name, presence: true
  validates_inclusion_of :is_admin, in: [true, false]
  validates :password,
            length: { minimum: 6 },
            if: -> { new_record? || !password.nil? }
end
