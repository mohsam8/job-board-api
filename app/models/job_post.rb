class JobPost < ApplicationRecord
  has_many :job_applications
  validates :title, presence: true
  validates :description, presence: true
end
