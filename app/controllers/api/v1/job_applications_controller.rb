class Api::V1::JobApplicationsController < ApplicationController

  #get api/v1/job_applications/
  def index
    if @current_user.is_admin
      @job_applications = JobApplication.all
    else
      @job_applications = JobApplication.where(user_id: @current_user)
    end
    render json: @job_applications
  end

  #get api/v1/@job_applications/:id
  def show
    @job_application = JobApplication.find(params[:id])
    authorize @job_application
    render json: @job_application
  end

  #post api/v1/@job_applications/
  def create
    if JobApplication.exists?(user_id: @current_user.id, job_post_id: job_application_params[:job_post_id])
      render json: { error: 'Unable to create JobApplication' }, status: 400
    else
      @job_application = JobApplication.new(job_application_params.merge(user_id: @current_user.id, status: "not_seen"))
      authorize @job_application
      if @job_application.save
        render json: @job_application
      else
        render json: { error: 'Unable to create JobApplication' }, status: 400
      end
    end
  end

  #put api/v1/@job_applications/:id
  def update
    @job_application = JobApplication.find(params[:id])
    authorize @job_application
    if @job_application
      @job_application.update(job_application_params)
      render json: { job_application: @job_application, message: "Job Application successfully updated" }
    else
      render json: { error: 'Unable to update JobApplication' }, status: 400
    end
  end

  #delete api/v1/@job_applications/:id
  def destroy
    @job_application = JobApplication.find(params[:id])
    authorize @job_application
    if @job_application
      @job_application.destroy
      render json: { message: "Job Application successfully destroy" }
    else
      render json: { error: 'Unable to destroy JobApplication' }, status: 400
    end
  end

  private

  def job_application_params
    params.permit(:job_post_id, :status)
  end
end
