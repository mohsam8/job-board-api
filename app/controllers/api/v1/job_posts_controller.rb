class Api::V1::JobPostsController < ApplicationController

  #get api/v1/job_posts/
  def index
    @job_posts = JobPost.where("expiry_date >= ?", Date.today)
    if params[:title]
      @job_posts = @job_posts.where("title like ?", "%#{params[:title]}%")
    end
    if params[:start_date] or params[:end_date]
      start_date = params[:start_date].to_datetime
      end_date = params[:end_date].to_datetime
      @job_posts = @job_posts.where("created_at between (?) and (?)", start_date, end_date)
    end
    authorize @job_posts
    render json: @job_posts
  end

  #get api/v1/job_posts/:id
  def show
    @job_post = JobPost.find(params[:id])
    authorize @job_post
    render json: @job_post
  end

  #post api/v1/job_posts/
  def create
    @job_post = JobPost.new(job_post_params)
    authorize @job_post
    if @job_post.save
      render json: { job_post: @job_post, message: "JobPost successfully created" }
    else
      render error: { error: 'Unable to create JobPost', status: 400 }
    end
  end

  #pit api/v1/job_posts/:id
  def update
    @job_post = JobPost.find(params[:id])
    authorize @job_post
    if @job_post
      @job_post.update(job_post_params)
      render json: { job_post: @job_post, message: "JobPost successfully updated" }
    else
      render error: { error: 'Unable to update JobPost', status: 400 }
    end
  end

  #delete api/v1/job_posts/:id
  def destroy
    @job_post = JobPost.find(params[:id])
    authorize @job_post
    if @job_post
      @job_post.destroy
      render json: { message: "Job Post successfully destroy" }
    else
      render error: { error: 'Unable to destroy Job Post', status: 400 }
    end
  end

  private

  def job_post_params
    params.permit(:title, :description)
  end
end
