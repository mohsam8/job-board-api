class Api::V1::UsersController < ApplicationController

  #get api/v1/users/
  def index
    @users = User.all
    render json: @users
  end

  #get api/v1/users/:id
  def show
    @user = User.find(params[:id])
    render json: @user
  end

  #post api/v1/users/
  def create
    @user = User.new(user_params)
    if @user.save
      render json: @user
    else
      render json: { error: 'Unable to create User' }, status: 400
    end
  end

  #put api/v1/users/:id
  def update
    @user = User.find(params[:id])

    if @user
      @user.update(user_params)
      render json: @user, message: "User successfully updated"
    else
      render error: { error: 'Unable to update User', status: 400 }
    end
  end

  private
  def user_params
    params.permit(:email, :password, :is_admin, :name)
  end
end
