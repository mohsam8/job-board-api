class AuthenticationController < ApplicationController

  # POST /auth/login
  def login
    @user = User.find_by_email(params[:email])
    if @user&.authenticate(params[:password])
      token = JsonWebToken.encode(user_id: @user.id)
      time = Time.now + 24.hours.to_i
      render json: { token: token, exp: time.strftime("%m-%d-%Y %H:%M"),
                     email: @user.email }, status: :ok
    else
      render json: { error: 'unauthorized' }, status: :unauthorized
    end
  end

  def signup
    @user = User.new(user_params)
    if @user.save
      render json: @user
    else
      render json: { error: 'Unable to create User' }, status: 400
    end
  end

  private
  def login_params
    params.permit(:email, :password)
  end

  private
  def user_params
    params.permit(:email, :password, :is_admin, :name)
  end
end