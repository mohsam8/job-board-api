Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :users do
          resources :job_applications do
            resources :job_posts
          end
      end
      resources :job_posts do
        resources :job_applications do
          resources :users
        end
      end
      resources :job_applications do
        resources :job_applications
        resources :users
      end
    end
  end
  resources :users, param: :email
  post '/auth/login', to: 'authentication#login'
  post '/auth/signup', to: 'authentication#signup'
  get '/*a', to: 'application#not_found'
end
