# README

This project is a small implementation for a Job board api. this is a restful Api that uses ruby on rails.

To run it after install to you need to:

install the gemfile by running:

    bundle install

then start the server:

    rails start

this project contains three main models (User, JobPost and JobApplication), user with admin role ("is_admin") can create mange the JobPosts  the other users can apply to a jobPost the the admin can reed the application.


here is some of the requests that you can user:
1) Login: it needs an email and a password to return sa Jwt token :
   

    post: auth/login

2) Signup: it needs an email, name, password and is_admin to create a user :


    post: auth/signup

3) all the basic requests for job_posts that include (index, show, create, update and destroy):


    api/v1/job_posts/


3) all the basic requests for job_applications that include (index, show, create, update and destroy):


    api/v1/job_applications/


